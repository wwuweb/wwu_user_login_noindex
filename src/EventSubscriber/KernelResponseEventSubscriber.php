<?php

namespace Drupal\wwu_user_login_noindex\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Requeststack;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

final class KernelResponseEventSubscriber implements EventSubscriberInterface {

  private $requestStack;

  public function __construct(RequestStack $request_stack) {
    $this->requestStack = $request_stack;
  }

  public function onResponse(ResponseEvent $event) {
    $response = $event->getResponse();
    $request = $this->requestStack->getCurrentRequest();
    $uri = $request->getRequestUri();

    switch ($uri) {
      case '/user/login':
        $response->headers->set('X-Robots-Tag', 'none, noimageindex');
    }
  }

  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = ['onResponse'];

    return $events;
  }

}
